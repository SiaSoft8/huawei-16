
var H= document.documentElement.clientHeight;
var a = $('.navbar-default').height();
var bottomHeight =H-a-2;
$(".pic-show").height(bottomHeight-$('.CompositeIndicator').height()-30);

$('.left-bar').height(bottomHeight);

var myDate = new Date();
var year = myDate.getFullYear();
var month = myDate.getMonth()+2;
var date1 = myDate.getDate();

function getDate() {
    month--;
    if(month==0){
        month=12;
        year--;
    }

    return year+'-'+month+'-'+date1;
}

function test(){

    if($('.right-box').hasClass('parent')){
        $(".right-box").css("border-radius","2px").animate({width:"660px"});
        $(".right-box").removeClass("parent");
        $(".right-box").html("<div class='container'>" +
            "<div class='modal-header'>" +
            "<button type='button'  onclick='close()' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>" +
            "<h4 class='modal-title'>数据信息</h4>" +
            "</div>"+
            "<div class='row'>" +
            "<table class='table'>" +
            "<thead>" +
            "<tr>" +
            "<th>时间</th>" +
            "<th>供水温度(°C)</th>" +
            "<th>风机数当量(个)</th>" +
            "<th>电泵数当量(个)</th>" +
            "<th>回水温度(°C)</th>" +
            "<th>最低流量(t/h)</th>" +
            "</tr>" +
            "</thead>" +
            "<tbody>" +
            "<tr>" +
            "<td>"+getDate()+"</td>" +
            "<td>"+Math.floor(Math.random()*5 + 30)+"</td>" +
            "<td>"+Math.floor(Math.random()*3 + 9)+"</td>" +
            "<td>"+Math.floor(Math.random()*1 + 2)+"</td>" +
            "<td>"+(Math.random()*6 + 29).toFixed(2)+"</td>" +
            "<td>"+Math.floor(Math.random()*10000 + 30000)+"</td>" +
            "</tr>" +
            "<tr>" +
            "<td>"+getDate()+"</td>" +
            "<td>"+Math.floor(Math.random()*5 + 30)+"</td>" +
            "<td>"+Math.floor(Math.random()*3 + 9)+"</td>" +
            "<td>"+Math.floor(Math.random()*1 + 2)+"</td>" +
            "<td>"+(Math.random()*6 + 29).toFixed(2)+"</td>" +
            "<td>"+Math.floor(Math.random()*10000 + 30000)+"</td>" +
            "</tr>" +
            "<tr>" +
            "<td>"+getDate()+"</td>" +
            "<td>"+Math.floor(Math.random()*5 + 30)+"</td>" +
            "<td>"+Math.floor(Math.random()*3 + 9)+"</td>" +
            "<td>"+Math.floor(Math.random()*1 + 2)+"</td>" +
            "<td>"+(Math.random()*6 + 29).toFixed(2)+"</td>" +
            "<td>"+Math.floor(Math.random()*10000 + 30000)+"</td>" +
            "</tr>" +
            "<tr>" +
            "<td>"+getDate()+"</td>" +
            "<td>"+Math.floor(Math.random()*5 + 30)+"</td>" +
            "<td>"+Math.floor(Math.random()*3 + 9)+"</td>" +
            "<td>"+Math.floor(Math.random()*1 + 2)+"</td>" +
            "<td>"+(Math.random()*6 + 29).toFixed(2)+"</td>" +
            "<td>"+Math.floor(Math.random()*10000 + 30000)+"</td>" +
            "</tr>" +
            "<tr>" +
            "<td>"+getDate()+"</td>" +
            "<td>"+Math.floor(Math.random()*5 + 30)+"</td>" +
            "<td>"+Math.floor(Math.random()*3 + 9)+"</td>" +
            "<td>"+Math.floor(Math.random()*1 + 2)+"</td>" +
            "<td>"+(Math.random()*6 + 29).toFixed(2)+"</td>" +
            "<td>"+Math.floor(Math.random()*10000 + 30000)+"</td>" +
            "</tr>" +
            "<tr>" +
            "<td>"+getDate()+"</td>" +
            "<td>"+Math.floor(Math.random()*5 + 30)+"</td>" +
            "<td>"+Math.floor(Math.random()*3 + 9)+"</td>" +
            "<td>"+Math.floor(Math.random()*1 + 2)+"</td>" +
            "<td>"+(Math.random()*6 + 29).toFixed(2)+"</td>" +
            "<td>"+Math.floor(Math.random()*10000 + 30000)+"</td>" +
            "</tr>" +
            "<tr>" +
            "<td>"+getDate()+"</td>" +
            "<td>"+Math.floor(Math.random()*5 + 30)+"</td>" +
            "<td>"+Math.floor(Math.random()*3 + 9)+"</td>" +
            "<td>"+Math.floor(Math.random()*1 + 2)+"</td>" +
            "<td>"+(Math.random()*6 + 29).toFixed(2)+"</td>" +
            "<td>"+Math.floor(Math.random()*10000 + 30000)+"</td>" +
            "</tr>" +
            "<tr>" +
            "<td>"+getDate()+"</td>" +
            "<td>"+Math.floor(Math.random()*5 + 30)+"</td>" +
            "<td>"+Math.floor(Math.random()*3 + 9)+"</td>" +
            "<td>"+Math.floor(Math.random()*1 + 2)+"</td>" +
            "<td>"+(Math.random()*6 + 29).toFixed(2)+"</td>" +
            "<td>"+Math.floor(Math.random()*10000 + 30000)+"</td>" +
            "</tr>" +
            "<tr>" +
            "<td>"+getDate()+"</td>" +
            "<td>31</td>" +
            "<td>9</td>" +
            "<td>2</td>" +
            "<td>39.74</td>" +
            "<td>40159</td>" +
            "</tr>" +
            "</tbody>" +
            "</table>" +
            "</div>" +
            "</div>");
    }else{
        $(".right-box").css("border-radius","50% 0 0 50%").animate({width:"3%"},50);
        $('.right-box').addClass('parent');
        $(".right-box").html('点<br>击<br>可<br>查<br>看<br>更<br>多<br>数<br>据<div class="triangle-left"></div>');
    }
}

function show(){
    $(".secondMenu").removeClass("hidden").addClass("show")
}
function none(){
    $(".secondMenu").removeClass("show").addClass("hidden")

}


$('.on-off-wind .col-sm-2:nth-child(6)').click(function(){
    if($(this).hasClass('turn-red')){
        $(this).removeClass('turn-red').addClass('turn-green');
    }else if($(this).hasClass('turn-green')){
        $(this).removeClass('turn-green').addClass('turn-red');
    }

    var m=$('.on-off-wind .turn-green').length;
    $('.num-box-wind .data').text(m);

    if(m == 10){

        myChart22.setOption({
            series: [
                {
                    name:'Step Start',
                    type:'line',
                    data:[20, 32, 28, 34, 60, 30, 10,15,26,45,24,35, 52, 23, 56, 45, 21 ,32, 55, 12, 56, 34 ,25,27,31],
                }
            ]
        });
    }else if(m == 11){

        myChart22.setOption({
            series: [
                {
                    name:'Step Start',
                    type:'line',
                    data:[25, 31, 29, 24, 40, 50, 20,25,36,44,24,38, 42, 28, 46, 49, 21 ,32, 55, 14, 51, 24 ,35,27,39],
                }
            ]
        });
    }else{

        myChart22.setOption({
            series: [
                {
                    name:'Step Start',
                    type:'line',
                    data:[24, 36, 39, 24, 43, 30, 20,27,46,41,24,48, 41, 28, 46, 49, 21 ,32, 51, 34, 41, 34 ,25,37,49],
                }
            ]
        });
    }

});

var myChart2 = echarts.init(document.getElementById('tem'));

function refresh2(){

    option = {
        title : {
            text: '温度和湿度',
            textStyle:{
                color:'#fff',
                fontSize:17
            }
        },
        grid: {
            left: '1%',
            right: 0,
            bottom: '2%',
            top:'25%',
            containLabel: true
        },
        tooltip : {
            trigger: 'axis'
        },
        legend: {
            x:'right',
            data:[{name:'温度',textStyle:{color:'#fff'}},{name:'湿度',textStyle:{color:'#fff'}}]
        },
        calculable : true,
        xAxis : [
            {
                type : 'category',
                boundaryGap : false,//是否从零点开始
                data : ['0','1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24'],
                axisLabel : {
                    formatter: '{value} :00'
                },
                axisLine:{
                    lineStyle:{
                        color:'#aaa'
                    }
                }
            }
        ],
        yAxis : [
            {
                type : 'value',
                name:'温度',
                axisLabel : {
                    formatter: '{value} °C'
                },
                axisLine:{
                    lineStyle:{
                        color:'#aaa'
                    }
                }
            },
            {
                type : 'value',
                name:'湿度',
                min:0,
                axisLabel : {
                    formatter: '{value} %'
                },
//                                                                    splitArea : {show : true},
                axisLine:{
                    lineStyle:{
                        color:'#aaa'
                    }
                }
            }
        ],
        series : [
            {
                name:'温度',
                type:'line',
                itemStyle: {
                    normal: {
                        lineStyle: {
                            color:'#D9534F',
                            shadowColor : 'rgba(0,0,0,0.4)',
                            width:2
                        }
                    }
                },
                data:[-8, -8, -8, -8, -9, -10, -5,-2,0,1,2,3,4,4,5,5,2,1,0,-3,-5,-6,-7,-7,-8]
            },
            {
                name:'湿度',
                type:'line',
                yAxisIndex: 1,
                itemStyle: {
                    normal: {
                        lineStyle: {
                            color:'#5CB85C',
                            shadowColor : 'rgba(0,0,0,0.4)',
                            width:2
                        }
                    }
                },
                data:[10,20,50,60,50,34,35,56,63,34,34,56,34,4,24,24,24,54,25,14,51,23,29,23,32]
            }
        ]
    };

    // 为echarts对象加载数据
    myChart2.setOption(option);

}

refresh2();


var myChart22 = echarts.init(document.getElementById('wt'));

function refresh22(){

    option2 = {
        title: {
            text: '一天内回水温度',
            textStyle:{
                color:'#fff',
                fontSize:17
            }
        },
        tooltip: {
            trigger: 'axis'
        },
        grid: {
            left: '1%',
            right: '2%',
            bottom: '1%',
            top:'15%',
            containLabel: true
        },
        xAxis: {
            type: 'category',
            boundaryGap : false,//是否从零点开始
            data : ['0','1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24'],
            axisLabel : {
                formatter: '{value} :00'
            },
            axisLine:{
                lineStyle:{
                    color:'#aaa'
                }
            }
        },
        yAxis: {
            type: 'value',
            axisLabel : {
                formatter: '{value} °C'
            },
            axisLine:{
                lineStyle:{
                    color:'#aaa'
                }
            }
        },
        series: [
            {
                name:'Step Start',
                type:'line',
                step: 'start',
                data:[20, 32, 28, 34, 60, 30, 10,15,26,45,24,35, 52, 23, 56, 45, 21 ,32, 55, 12, 56, 34 ,25,27,31],
                itemStyle: {
                    normal: {
                        lineStyle: {
                            color:'#D9534F',
                            shadowColor : 'rgba(0,0,0,0.4)',
                            width:2
                        }
                    }
                }
            }
        ]
    };

    // 为echarts对象加载数据
    myChart22.setOption(option2);

}

refresh22();




// $('.on-off-water .col-sm-2').click(function(){
//     if($(this).hasClass('turn-red')){
//         $(this).removeClass('turn-red').addClass('turn-green');
//         var n=$('.on-off-water .turn-green').length;
//         $('.num-box-water .data').text(n);
//     }else if($(this).hasClass('turn-green')){
//         $(this).removeClass('turn-green').addClass('turn-red');
//         var m=$('.on-off-water .turn-green').length;
//         $('.num-box-water .data').text(m);
//     }
//
// });



$('.definition').click(function(){
    $('.s-moveBar').fadeIn();
});
jQuery(document).ready( // 可拖拽框
    function () {
        $('.s-moveBar .s-banner').mousedown(
            function (event) {
                var isMove = true;
                var abs_x = event.pageX - $('div.s-moveBar').offset().left;
                var abs_y = event.pageY - $('div.s-moveBar').offset().top;
                $(document).mousemove(function (event) {
                        if (isMove) {
                            var obj = $('div.s-moveBar');
                            obj.css({'left':event.pageX - abs_x, 'top':event.pageY - abs_y});
                        }
                    }
                ).mouseup(
                    function () {
                        isMove = false;
                    }
                );
            }
        );
    }
);
$('.s-moveBar').on('click','.close,.modelCancel',function(){
    $('.s-moveBar').fadeOut();
});


$(function() {
    $( "#slider-range-max" ).slider({
        orientation: "vertical",
        min: 1,
        max: 24,
        slide: function( event, ui ) {
            $( "#amount" ).val( ui.value );
        }
    });
    $( "#amount" ).val( $( "#slider-range-max" ).slider( "value") );
});











