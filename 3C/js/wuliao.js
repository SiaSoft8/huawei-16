//生产时间
var product = 2;
//物料状态
var wuliao = ['物料充足','补充中...'];
var material = ['C5110','C5210','C18070','C7025','C14415','cuni2si','C19010','C7541','C18400','C68800'];

$('#1').text(material[0]);

$('.rukuxinghao .div .third-30:nth-child(3)').each(function(){
    $(this).text(material[Math.floor(Math.random()*material.length)]);
});
$('.wuliaoPlan .div .sixth:nth-child(2)').each(function(){
    $(this).text(material[Math.floor(Math.random()*material.length)]);
});

//能耗监控
var myChart1 = echarts.init(document.getElementById('echart1-1'));
function refresh1() {
    option = {
        tooltip : {
            trigger: 'item',
            formatter: "{a} <br/>{b} : {c} ({d}%)"
        },

        visualMap: {
            show: false,
            min: 80,
            max: 600,
            inRange: {
                colorLightness: [0, 1]
            }
        },
        series : [
            {
                name:'物料监控',
                type:'pie',
                radius : '85%',
                center: ['50%', '50%'],
                data:[
                    {value:335, name:material[0]},
                    {value:310, name:material[1]},
                    {value:274, name:material[2]},
                    {value:235, name:material[3]},
                    {value:400, name:material[4]},
                    {value:300, name:material[5]},
                    {value:250, name:material[6]},
                    {value:360, name:material[7]},
                    {value:420, name:material[8]},
                    {value:340, name:material[9]}
                ].sort(function (a, b) { return a.value - b.value; }),
                roseType: 'radius',
                label: {
                    normal: {
                        textStyle: {
                            color: 'rgba(255, 255, 255, 0.3)'
                        }
                    }
                },
                labelLine: {
                    normal: {
                        lineStyle: {
                            color: 'rgba(255, 255, 255, 0.3)'
                        },
                        smooth: 0.2,
                        length: 10,
                        length2: 20
                    }
                },
                itemStyle: {
                    normal: {
                        color: '#c23531',
                        shadowBlur: 200,
                        shadowColor: 'rgba(0, 0, 0, 0.5)'
                    }
                },

                animationType: 'scale',
                animationEasing: 'elasticOut',
                animationDelay: function (idx) {
                    return Math.random() * 200;
                }
            }
        ]
    };

    // 为echarts对象加载数据
    myChart1.setOption(option);

}
refresh1();



// setInterval(function(){
//     $('.wuliao').each(function(){
//         var nu = Math.random()*2;
//         if( nu > 0.1 ){
//             $(this).removeClass('s-error').addClass('s-normal').text(wuliao[0]);
//         }else{
//             $(this).removeClass('s-normal').addClass('s-error').text(wuliao[1]);
//         }
//
//     })
// },product*1000);